﻿using Cinemachine;
using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// Bolt extensions class that allows to set X Axis value and Follow Offset X of <see cref="CinemachineOrbitalTransposer">
    /// </summary>
    public class CMOrbitalTransposerPropertiesSetter : MonoBehaviour
    {
        public void SetProperties(CinemachineVirtualCamera camera, int priority, bool recenteringEnabled)
        {
            var transposer = camera.GetCinemachineComponent<CinemachineOrbitalTransposer>();

            if (transposer == null) 
            {
                return;
            }

            camera.Priority = priority;
            transposer.m_RecenterToTargetHeading.m_enabled = recenteringEnabled;
        }
    }
}
